-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2021 at 08:23 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `accounts`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`userID` bigint(20) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userFirstName` varchar(25) NOT NULL,
  `userLastName` varchar(25) NOT NULL,
  `userPassword` varchar(32) NOT NULL,
  `userPasswordAttempts` int(1) NOT NULL DEFAULT '0',
  `userPrevPassword` text,
  `userPasswordDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userCreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userRole` int(2) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `userActive` int(1) NOT NULL DEFAULT '0',
  `userActiveDate` text,
  `userForgot` varchar(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1000011 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `userEmail`, `userFirstName`, `userLastName`, `userPassword`, `userPasswordAttempts`, `userPrevPassword`, `userPasswordDate`, `userCreationDate`, `userRole`, `created_by`, `userActive`, `userActiveDate`, `userForgot`) VALUES
(477, 'sujon@bd.com', 'Sujon', 'Islam', 'e10adc3949ba59abbe56e057f20f883e', 9, NULL, '2020-02-17 09:01:51', '2020-02-17 09:01:51', 10, NULL, 1, NULL, NULL),
(1000006, 'tofazzul@hussainsbd.com', 'Tofazzul ', 'Hussain', '202cb962ac59075b964b07152d234b70', 0, '["1a996d6f57e2b348b6531264274edd2f"]', '2020-03-10 06:14:53', '2020-03-10 06:14:53', 0, NULL, 1, NULL, NULL),
(1000007, 'abd@gmail.com', 'ABD', 'A', 'e10adc3949ba59abbe56e057f20f883e', 3, '["cae558648534d51deb527140c720a29a",null]', '2020-03-10 08:30:24', '2020-03-10 08:30:24', 0, NULL, 1, NULL, NULL),
(1000008, 'sa.sayeed@hussainsbd.com', 'Sheikh', 'Abu Sayeed', '202cb962ac59075b964b07152d234b70', 0, '["857a8b6a3df522b465cccd65bfdc4186",null]', '2020-03-12 06:49:53', '2020-03-12 06:49:53', 0, NULL, 1, NULL, NULL),
(1000009, 'hbcl@hussainsbd.com', 'HBCL', '-', 'e10adc3949ba59abbe56e057f20f883e', 1, '["70ff30b6ffbc01d87f9939b3f3f57b16",null,null]', '2020-03-16 08:50:35', '2020-03-16 08:50:35', 0, NULL, 1, NULL, 'da96ca5541c3caa2b822ec7315f867d3'),
(1000010, 'puma@gmail.com', 'Puma', 'ABC', '65743241172c9d73b0cfc5bf43938bd9', 0, '["65743241172c9d73b0cfc5bf43938bd9",null]', '2020-03-18 07:42:30', '2020-03-18 07:42:30', 0, NULL, 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`userID`), ADD UNIQUE KEY `userEmail` (`userEmail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `userID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1000011;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
